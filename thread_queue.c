/*
  Queue libirary for thread

  $Id$
*/

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <errno.h>

#include "thread_queue.h"

struct tq_t *
tq_alloc(char *name)
{
  return tq_alloc_pri(name, 1) ;
}

struct tq_t *
tq_alloc_pri(char *name, int npri)
{
  int i ;
  int sz = sizeof(struct tq_t) + sizeof(struct tailq_head_t) * (npri-1) ;
  struct tq_t *p = (struct tq_t *)malloc(sz);

  if (p)
    {
      bzero(p, sz) ;
      pthread_mutex_init(&TQ_MUTEX(p), NULL) ;
      pthread_cond_init(&TQ_COND(p), NULL) ;
      TQ_NPRI(p) = npri ;
      for(i=0 ; i<npri ; i++)
        TAILQ_INIT(&TQ_LINKPRI(p, i)) ;
      TAILQ_INIT(&TQ_FREE_LINK(p)) ;
      TQ_NAME(p) = name ? strdup(name) : NULL ;
    }
  return p ;
}

void
tq_free(struct tq_t *ptr)
{
  if (TQ_NAME(ptr))
    free(TQ_NAME(ptr)) ;
  free(ptr) ;
}

int
tq_lock(struct tq_t *ptr)
{
  return pthread_mutex_lock(&TQ_MUTEX(ptr)) ;
}

int
tq_unlock(struct tq_t *ptr)
{
  return (pthread_cond_broadcast(&TQ_COND(ptr)) == 0
	  && pthread_mutex_unlock(&TQ_MUTEX(ptr)) == 0) ? 0 : -1 ;
}


/*
 */
#define ELM_BUF_ADDR(p)							\
  ((TQ_ELM_LEN(p) > MAX_TQ_ELEMENT_SIZE) ? TQ_ELM_PTR(p) : TQ_ELM_BUF(p))

struct tq_elm_t *
alloc_tq_elm(struct tq_t *tq, void *buf, size_t len)
{
  struct tq_elm_t *ptr ;

  if (TQ_FREE_NELM(tq) > 0)
    {
      ptr = TAILQ_FIRST(&TQ_FREE_LINK(tq)) ;
      TAILQ_REMOVE(&TQ_FREE_LINK(tq), ptr, link) ;
      TQ_FREE_NELM(tq)-- ;
    }
  else
    ptr = (struct tq_elm_t *)malloc(sizeof(struct tq_elm_t)) ;

  if (ptr)
    {
      //bzero(&TQ_ELM_LINK(ptr), sizeof(struct tq_elm_t)) ;
      bzero(ptr, sizeof(struct tq_elm_t)) ;
      if (len > 0 && len > MAX_TQ_ELEMENT_SIZE)
	{
	  TQ_ELM_PTR(ptr) = (char *)malloc(len) ;
	}
      TQ_ELM_LEN(ptr) = len ;
    }
  return ptr ;
}

void
free_tq_elm(struct tq_t *tq, struct tq_elm_t *ptr)
{
  if (TQ_ELM_LEN(ptr) > MAX_TQ_ELEMENT_SIZE)
    {
      free(TQ_ELM_PTR(ptr)) ;
      TQ_ELM_PTR(ptr) = NULL ;
    }
  TAILQ_INSERT_TAIL(&TQ_FREE_LINK(tq), ptr, link) ;
  TQ_FREE_NELM(tq)++ ;
  //free(ptr) ;
}

/*
  int tq_enqueue(struct tq_t *tq, void *buf, size_t len)
  int tq_dequeue(struct tq_t *tq, void *buf)
  int tq_dequeue_reltime(struct tq_t *tq, void *buf, struct timespec *reltime)
  int tq_dequeue_abstime(struct tq_t *tq, void *buf, struct timespec *abstime)
  int tq_count(struct tq_t *tq)
  int tq_free_count(struct tq_t *tq)
*/
int
tq_count(struct tq_t *tq)
{
  int count ;
  tq_lock(tq) ;
  count = TQ_NELM(tq) ;
  tq_unlock(tq) ;
  return count ;
}

int
tq_free_count(struct tq_t *tq)
{
  int count ;
  tq_lock(tq) ;
  count = TQ_FREE_NELM(tq) ;
  tq_unlock(tq) ;
  return count ;
}

#if 0
int
tq_enqueue(struct tq_t *tq, void *buf, size_t len)
{
  int ret = 0 ;

  if (tq_lock(tq) == 0)
    {
      struct tq_elm_t *elm = alloc_tq_elm(tq, buf, len) ;

      bcopy(buf, ELM_BUF_ADDR(elm), len) ;

      TAILQ_INSERT_TAIL(&TQ_LINK(tq), elm, link) ;
      TQ_NELM(tq)++ ;
      pthread_cond_broadcast(&TQ_COND(tq)) ;
      tq_unlock(tq) ;
      ret = len ;
    }
  return ret ;
}
#endif
int
tq_enqueue(struct tq_t *tq, void *buf, size_t len)
{
  return tq_enqueue_pri(tq, buf, len, 0) ;
}

int
tq_enqueue_pri(struct tq_t *tq, void *buf, size_t len, int pri)
{
  int ret = 0 ;

  if (tq_lock(tq) == 0)
    {
      while(TQ_NELM_MAX(tq) > 0 && TQ_NELM(tq) >= TQ_NELM_MAX(tq))
	{
	  ret = pthread_cond_wait(&TQ_COND(tq), &TQ_MUTEX(tq)) ;
	  if (ret == ETIMEDOUT)
	    {
	      break ;
	    }
	}

      len == 0 ;
      if (ret == 0)
	{
          struct tq_elm_t *elm = alloc_tq_elm(tq, buf, len) ;

          bcopy(buf, ELM_BUF_ADDR(elm), len) ;

          TAILQ_INSERT_TAIL(&TQ_LINKPRI(tq, pri), elm, link) ;
          TQ_NELM(tq)++ ;
	  pthread_cond_broadcast(&TQ_COND(tq)) ;
	}
      tq_unlock(tq) ;
      ret = len ;
    }
  return ret ;
}

int
tq_dequeue(struct tq_t *tq, void *buf)
{
  struct tq_elm_t *ptr ;
  int len = 0 ;
  int r = tq_dequeue_1(tq, &ptr) ;

  if (r == 0)
    if (ptr != NULL)
      {
	len = TQ_ELM_LEN(ptr) ;
	bcopy(ELM_BUF_ADDR(ptr), buf, len) ;
        tq_lock(tq) ;
        free_tq_elm(tq, ptr) ;
        tq_unlock(tq) ;
      }
    else
      abort() ;
  else /* if (r == ETIMEDOUT) */
    len = -r ;
  return len ;
}

int
tq_dequeue_reltime(struct tq_t *tq, void *buf, struct timespec *reltime)
{
  struct tq_elm_t *ptr ;
  int len = 0 ;
  int r = tq_dequeue_reltime_1(tq, &ptr, reltime) ;

  if (r == 0)
    if (ptr != NULL)
      {
	len = TQ_ELM_LEN(ptr) ;
	bcopy(ELM_BUF_ADDR(ptr), buf, len) ;
        tq_lock(tq) ;
        free_tq_elm(tq, ptr) ;
        tq_unlock(tq) ;
      }
    else
      abort() ;
  else /* if (r == ETIMEDOUT) */
    len = -r ;
  return len ;
}

int
tq_dequeue_abstime(struct tq_t *tq, void *buf, struct timespec *abstime)
{
  struct tq_elm_t *ptr ;
  int len = 0 ;
  int r = tq_dequeue_abstime_1(tq, &ptr, abstime) ;

  if (r == 0)
    if (ptr != NULL)
      {
	len = TQ_ELM_LEN(ptr) ;
	bcopy(ELM_BUF_ADDR(ptr), buf, len) ;
        tq_lock(tq) ;
        free_tq_elm(tq, ptr) ;
        tq_unlock(tq) ;
      }
    else
      abort() ;
  else /* if (r == ETIMEDOUT) */
    len = -r ;
  return len ;
}

/*
  int tq_dequeue_1(struct tq_t *tq, struct tq_elm_t **elm)
  int tq_dequeue_reltime_1(struct tq_t *tq, struct tq_elm_t **elm, struct timespec *reltime)
  int tq_dequeue_abstime_1(struct tq_t *tq, struct tq_elm_t **elm, struct timespec *abstime)
  return 0 on success, else return ETIMEDOUT(see errno.h).
 */
int
tq_dequeue_1(struct tq_t *tq, struct tq_elm_t **elm)
{
  struct timespec abstime = { INT_MAX, 0 } ;

  return tq_dequeue_abstime_1(tq, elm, &abstime) ;
}

int
tq_dequeue_reltime_1(struct tq_t *tq, struct tq_elm_t **elm, struct timespec *reltime)
{
  int ret ;
  struct timespec abstime ;
  struct timeval tv_now ;
  struct timespec ts_now ;

  gettimeofday(&tv_now, NULL) ;
  TIMEVAL_TO_TIMESPEC(&tv_now, &ts_now) ;
  timespecadd(&ts_now, reltime, &abstime) ;

  return tq_dequeue_abstime_1(tq, elm, &abstime) ;
}

int
tq_dequeue_abstime_1(struct tq_t *tq, struct tq_elm_t **elm, struct timespec *abstime)
{
  int ret ;

  *elm = NULL ;
  ret = tq_lock(tq) ;
  if (ret == 0)
    {
      while(TQ_NELM(tq) == 0)
	{
	  ret = pthread_cond_timedwait(&TQ_COND(tq), &TQ_MUTEX(tq), abstime) ;
	  if (ret == ETIMEDOUT)
	    {
	      break ;
	    }
	}

      if (ret == 0)
	{
          int pri = TQ_NPRI(tq) - 1;
          struct tq_elm_t *p = TAILQ_FIRST(&TQ_LINKPRI(tq, pri)) ;
          while(p == NULL)
            {
              pri-- ;
              p = TAILQ_FIRST(&TQ_LINKPRI(tq, pri)) ;
            }
          if (p == NULL)
            abort() ;

	  *elm = p ;
          TAILQ_REMOVE(&TQ_LINKPRI(tq, pri), p, link) ;
          TQ_NELM(tq)-- ;
	  pthread_cond_broadcast(&TQ_COND(tq)) ;
	}
      (void)tq_unlock(tq) ;
    }
  return ret ;
}

/*
  ends
 */
/*
 * Local Variables:
 * mode: c
 * coding: utf-8-unix
 * End:
 */
