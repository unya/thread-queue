#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <errno.h>

#include "thread_queue.h"

int
main(int argc, char *argv[])
{
  int i ;
  struct tq_t *Q = tq_alloc_pri("no name", 2) ;
  char buf[4] ;

  snprintf(buf, 4, "%d", 0) ;
  tq_enqueue_pri(Q, buf, 4, 0) ;
  tq_enqueue_pri(Q, buf, 4, 0) ;

  snprintf(buf, 4, "%d", 1) ;
  tq_enqueue_pri(Q, buf, 4, 1) ;

  for(i=0 ; i<3 ; i++)
    {
      tq_dequeue(Q, buf) ;
      fprintf(stderr, "%d %s\n", i, buf) ;
    }
  exit(0) ;
}
