#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <errno.h>

#include "thread_queue.h"

void *
receiver(void *ptr)
{
  struct tq_t *Q = ptr ;

  printf("start recevier\n") ;
  while(1)
    {
      struct timespec tv = { 0, 999900000 } ;
      int len ;
      int i[16] ;
      void *buf = i ;

      printf("waitng\n") ;
      int r = tq_dequeue(Q, buf) ;
      //int r = tq_dequeue_reltime(Q, &buf, &tv) ;
      printf("received size=%d\n", r) ;

      if (r >= 0 && i[0]==-1)
	{
          printf("received exit %d\n", r) ;
	  break ;
	}
    }
  pthread_exit(NULL) ;
}

void *
sender(void *ptr)
{
  int i = 10 ;
  struct tq_t *Q = ptr ;
  int delay ;

  printf("start sender\n") ;

  srandom(time(0)) ;
  for(i=0 ; i<10 ; i++)
    {
      int len ;
      void *buf ;
      int r ;
      int ibuf[16] ;

      buf = ibuf ;
      r = tq_enqueue(Q, buf, sizeof(ibuf)) ;
#if 0
      sleep(1) ;
#else
      delay = random() % 2000000 ;
      printf("%d %d count(%d) delay %d\n", r, i, tq_count(Q), delay / 1000) ;
      usleep(delay) ;
#endif
    }
}


main()
{
  struct tq_t *Q ;
  pthread_t tid_r ;
  pthread_attr_t attr_r ;
  pthread_t tid_s[4] ;
  pthread_attr_t attr_s[4] ;
  int ibuf = -1 ;

  Q = tq_alloc("") ;

#if 1
  pthread_attr_init(&attr_r) ;
  pthread_create(&tid_r, &attr_r, receiver, Q) ;

  pthread_attr_init(&attr_s[0]) ;
  pthread_create(&tid_s[0], &attr_s[0], sender, Q) ;
#if 0
  pthread_attr_init(&attr_s[1]) ;
  pthread_create(&tid_s[1], &attr_s[1], sender, Q) ;
  pthread_attr_init(&attr_s[2]) ;
  pthread_create(&tid_s[2], &attr_s[2], sender, Q) ;
  pthread_join(tid_s[2], NULL) ;
  pthread_join(tid_s[1], NULL) ;
#endif
  pthread_join(tid_s[0], NULL) ;

  //pthread_attr_init(&attr_r) ;
  //pthread_create(&tid_r, &attr_r, receiver, Q) ;
#else
  sender(Q) ;
#endif
  tq_enqueue(Q, &ibuf, sizeof(int)) ;
  //tq_enqueue(Q, 0, 0) ;
  pthread_join(tid_r, NULL) ;
  tq_free(Q) ;
  exit(0) ;
}
