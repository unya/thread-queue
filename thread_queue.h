#ifndef TQ_H
#define TQ_H

/* $Id$ */

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <errno.h>

TAILQ_HEAD(tailq_head_t, tq_elm_t) ;

struct tq_t {
  pthread_mutex_t tq_mutex ;
  pthread_cond_t tq_cond ;
  char *tq_name ;
#if 0
  int tq_status ;
#endif
  size_t tq_nelm_max ;
  size_t tq_nelm ;
  size_t tq_free_nelm ;
  struct tailq_head_t tq_free_link ;
  int tq_npri ;
  struct tailq_head_t tq_head_link[1] ;
} ;

#define TQ_NAME(p) ((p)->tq_name)
#define TQ_NELM_MAX(p) ((p)->tq_nelm_max)
#define TQ_NELM(p) ((p)->tq_nelm)
#define TQ_FREE_NELM(p) ((p)->tq_free_nelm)
#define TQ_MUTEX(p) ((p)->tq_mutex)
#define TQ_COND(p) ((p)->tq_cond)
#define TQ_LINK(p) ((p)->tq_head_link[0])
#define TQ_LINKPRI(p, n) ((p)->tq_head_link[n])
#define TQ_FREE_LINK(p) ((p)->tq_free_link)
#define TQ_NPRI(p) ((p)->tq_npri)

#if 0
#define TQ_STATUS(p) ((p)->tq_status)
#endif

struct tq_t *tq_alloc(char *name) ;
struct tq_t *tq_alloc_pri(char *name, int npri) ;
void tq_free(struct tq_t *ptr) ;
int tq_lock(struct tq_t *ptr) ;
int tq_unlock(struct tq_t *ptr) ;

#ifndef MAX_TQ_ELEMENT_SIZE
#  define MAX_TQ_ELEMENT_SIZE sizeof(void *)
#endif

struct tq_elm_t {
  TAILQ_ENTRY(tq_elm_t) link;
  size_t len ;
  union {
    void *ptr ;
    char cbuf[MAX_TQ_ELEMENT_SIZE] ;
  } buf ;
} ;

#define TQ_ELM_LINK(p) ((p)->link)
#define TQ_ELM_LEN(p) ((p)->len)
#define TQ_ELM_BUF(p) ((p)->buf.cbuf)
#define TQ_ELM_PTR(p) ((p)->buf.ptr)


#ifndef TIMEVAL_TO_TIMESPEC
# define TIMEVAL_TO_TIMESPEC(tv, ts) {		\
    (ts)->tv_sec = (tv)->tv_sec;		\
    (ts)->tv_nsec = (tv)->tv_usec * 1000;	\
}
#endif

#ifndef timespecadd
#  define timespecadd(a, b, result)					\
  do {									\
    (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;			\
    (result)->tv_nsec = (a)->tv_nsec + (b)->tv_nsec;			\
    if ((result)->tv_nsec >= 1000000000)				\
      {									\
	++(result)->tv_sec;						\
	(result)->tv_nsec -= 1000000000;				\
      }									\
  } while (0)
#endif

/*
  int tq_enqueue(struct tq_t *tq, void *buf, size_t len)
  int tq_enqueue_pri(struct tq_t *tq, void *buf, size_t len, int pri)
  int tq_dequeue(struct tq_t *tp, void *buf)
  int tq_dequeue_reltime(struct tq_t *tp, void *buf, struct timespec *reltime)
  int tq_dequeue_abstime(struct tq_t *tp, void *buf, struct timespec *abstime)
  int tq_count(struct tq_t *tp)
*/
int tq_enqueue(struct tq_t *tq, void *buf, size_t len) ;
int tq_enqueue_pri(struct tq_t *tq, void *buf, size_t len, int pri);
int tq_dequeue(struct tq_t *tq, void *buf) ;
int tq_dequeue_reltime(struct tq_t *tq, void *buf, struct timespec *reltime) ;
int tq_dequeue_abstime(struct tq_t *tq, void *buf, struct timespec *abstime) ;
int tq_count(struct tq_t *tq) ;
int tq_free_count(struct tq_t *tq) ;

/*
  internal queue element operation
*/
struct tq_elm_t *alloc_tq_elm(struct tq_t *tq, void *buf, size_t len) ;
void free_tq_elm(struct tq_t *tq, struct tq_elm_t *ptr) ;
int tq_dequeue_1(struct tq_t *tq, struct tq_elm_t **elm) ;
int tq_dequeue_reltime_1(struct tq_t *tq, struct tq_elm_t **elm, struct timespec *reltime) ;
int tq_dequeue_abstime_1(struct tq_t *tq, struct tq_elm_t **elm, struct timespec *abstime) ;

#endif
/*
  ends
 */
/*
 * Local Variables:
 * mode: c
 * coding: utf-8-unix
 * End:
 */
